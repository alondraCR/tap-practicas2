/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.*;

/**
 *
 * @author Sukis
 */
public class Practica1 extends JFrame implements ActionListener{

    JLabel eti1;
    JTextField nombre;
    JButton b1;
    
    Practica1(){
        
        this.setTitle("Practica 1");
        this.setSize(300,150);
        this.setLayout(new FlowLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        eti1 = new JLabel("Escriba un nombre para saludar");
        nombre = new JTextField(20);
        b1 = new JButton("¡Saludar!");
        
        this.add(eti1);
        this.add(nombre);
        this.add(b1);
        
        b1.addActionListener(this);
        
    }
    
    
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
                new Practica1().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, "Hola"+this.nombre.getText());}
    
    
    
    
    
    
}
